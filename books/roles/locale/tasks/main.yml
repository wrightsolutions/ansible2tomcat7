---
# Playbook written for Ansible 1.2 compatibility
# Tag 'locally' refers to locale or any language specific checks / plays.
# When to use 'shell' rather than 'command'?
# (1) You want to use pipes (|) then use shell
#
# Ran into an issue with shell and multiple pipes (|) in Ansible 1.2 so
# workaround is to either use python on the .stdout or simplify
# so only uses max of 1 pipe
# Good: shell /usr/bin/locale | /bin/egrep '^(LANG|LC_ALL)'
# Good: shell /usr/bin/locale | /bin/egrep '^(LANG|LC_ALL)[[:print:]]'
# Fail: shell /usr/bin/locale | /bin/egrep '^(LANG|LC_ALL)[[:punct:]]e'
# Fail: shell /usr/bin/locale | /bin/egrep '^(LANG|LC_ALL).+en_GB.UTF-8'
# Fail: shell locale | grep -E '^(LANG|LC_ALL)' | grep 'GB'
# Fail: shell locale | grep -E '^(LANG|LC_ALL)' | grep 'en_GB.UTF-8'
# Fail: shell /usr/bin/locale | grep -E '^(LANG|LC_ALL)' | grep 'en_GB.UTF-8'
#
# The .stdout way might involve .stdout.find('en_GB.UTF-8') != -1 (found)
# The .stdout way might involve .stdout.find('en_GB.UTF-8') < 0 (not found) 
#
# Example check of return code numeric value from 'shell'
#   localegrep.rc == 1
#
# The stat module of Ansible 1.3 could be used to evaluate following two lines:
#  action: stat /usr/lib/locales-all
#  action: stat /usr/lib/locale/locale-archive

# Checking locale via /usr/bin/locale or printenv is far from trivial
# as Ansible probably forces a default LANG=C during remote connection
# Second choice / compromise is to grep /etc/default/locale file

- name: check locale is set as desired

  action: command /bin/egrep '^LANG=en_GB.UTF-8' /etc/default/locale
  #action: shell /usr/bin/locale | /bin/egrep '^(LANG|LC_ALL)'
  ignore_errors: True
  register: localegrep
  tags:
  - day0
  - locally

- name: information about remote locale (looks promising)
  action: debug msg="{{ localegrep }}"
  when: localegrep|success
  tags:
  - day0
  - locally

- name: information about remote locale - cat
  action: command /bin/cat /etc/default/locale
  register: localecat
  when: localegrep|failed
  tags:
  - day0
  - locally

- name: information about remote locale for debug
  action: debug msg="{{localecat.stdout_lines}}"
  when: localegrep|failed
  tags:
  - day0
  - locally

- name: check if locales-all installed
  action: shell /usr/bin/dpkg -l locales-all | egrep '^ii'
  ignore_errors: True
  register: allres
  when: localegrep|failed
  tags:
  - day0
  - locally

- name: check if locale-archive exists
  action: shell /usr/bin/du -h /usr/lib/locale/locale-archive
  ignore_errors: True
  register: archiveres
  when: localegrep|failed
  tags:
  - day0
  - locally

- name: information about size of locale-archive
  action: debug msg="{{ archiveres.stdout }}"
  when: localegrep|failed and archiveres.rc == 0
  tags:
  - day0
  - locally

- name: generate preferred locale in archive
  action: command /usr/sbin/locale-gen --archive en_GB.UTF-8
  when: localegrep|failed and allres.rc > 0 and archiveres.rc == 0
  tags:
  - day0
  - locally

- name: generate preferred locale
  action: command /usr/sbin/locale-gen en_GB.UTF-8
  when: localegrep|failed and allres.rc > 0 and archiveres.rc > 0
  tags:
  - day0
  - locally

- name: Send a sample locale default file into /etc/default/ regardless
  action: copy src=localeGB dest=/etc/default/ owner=root mode=0644
  tags:
  - day0
  - locally

- name: system default locale in /etc/default/ now updated (CHANGE unless skipped)
  action: copy src=localeGB dest=/etc/default/locale owner=root mode=0644
  when: localegrep|failed
  tags:
  - day0
  - locally
  - newonly

